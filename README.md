# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo contains my submission for the role at ClearSky Logic

### Assumptions ###

* The application did not need to be completed in full and must be done within a timeframe of 4 hours

### Language, Technologies and Frameworks Used ###

* C#
* Xamarin.Forms
* PRISM
* Firebase Database

### Limitations ###

* No authentication
* Registering interest in property is faked

Had I had more time on this project I would have integrated native implementations for Android and iOS for Firebase Authentication. This would allow for users to sign in with email and password or a variety of social accounts like Facebook or Google. I would have also used this actively signed in user data to customise the expierience for the user throughout their time in the app as well as automatically using that gathered data to streamline the property interest registering experience. I would have also of liked to include a picture gallery of the selected property.

There are also maintainability improvements I could have made such as using a shared resource dictionary for repeated UI elements such as colours or styling. I would have also liked the abstract the the navigation strings into a static file using constansts. This would have allowed me to guarantee navigation to the correct page without having to worry about syntax errors. 
﻿using CSLPropertyLetting.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CSLPropertyLetting.Interfaces
{
    public interface IStorageService
    {
        Task PopulateProperties();
        Task StoreFavourite(Favourite favourite);
        Task<List<Property>> GetAllProperties();
        Task<List<Favourite>> GetAllFavourites();
    }
}

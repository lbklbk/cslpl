﻿using CSLPropertyLetting.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CSLPropertyLetting.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PropertiesPage : ContentPage
    {
        public PropertiesPage()
        {
            InitializeComponent();
        }

        private async void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
           var p = e.Item as Property;
           await Navigation.PushModalAsync(new CreateEnquiryPage(p));
        }
    }
}
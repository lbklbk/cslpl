﻿using CSLPropertyLetting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CSLPropertyLetting.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateEnquiryPage : ContentPage
    {
        public CreateEnquiryPage(Property property)
        {
            InitializeComponent();
            StreetNumber.Text = property.StreetNumber.ToString();
            StreetName.Text = property.StreetName;
            Postcode.Text = property.Postcode;
            PropertyPrice.Text = property.PricePerMonth;
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}
﻿using CSLPropertyLetting.Models;
using Firebase.Database;
using Firebase.Database.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CSLPropertyLetting.Services
{
    public class DatabaseHelper
    {
        readonly FirebaseClient client = new FirebaseClient("https://cslletting.firebaseio.com/");

        public async Task<List<Property>> GatherProperties()
        {
            var props = await GetAllProperties();
            if(props.Count == 0)
            {
                await PopulateProperties();
            }
            else
            {
                return props;
            }

            var properties = await GetAllProperties();
            return properties;
        }

        public async Task<List<Property>> GetAllProperties()
        {
            return (await client.Child("Properties")
                .OnceAsync<Property>()).Select(property => new Property
                {
                    StreetNumber = property.Object.StreetNumber,
                    StreetName = property.Object.StreetName,
                    Postcode = property.Object.Postcode,
                    PricePerMonth = property.Object.PricePerMonth
                }).ToList();
        }

        public async Task PopulateProperties()
        {

            var propList = new List<Property>
            {
                new Property
                {
                    StreetNumber = 123,
                    StreetName = "Apple Street",
                    Postcode = "XY12 3AB",
                    PricePerMonth = "£730 pcm"
                },
                new Property
                {
                    StreetNumber = 4,
                    StreetName = "Privet Drive",
                    Postcode = "XY15 8GG",
                    PricePerMonth = "£495 pcm"
                },
                new Property
                {
                    StreetNumber = 7,
                    StreetName = "Cedar Drive",
                    Postcode = "XY43 7FG",
                    PricePerMonth = "£600 pcm"
                },
                new Property
                {
                    StreetNumber = 18,
                    StreetName = "Spruce Grove",
                    Postcode = "XY18 4CC",
                    PricePerMonth = "£590 pcm"
                },
                new Property
                {
                    StreetNumber = 4,
                    StreetName = "Pear Close",
                    Postcode = "XY20 9TF",
                    PricePerMonth = "£700 pcm"
                },
                new Property
                {
                    StreetNumber = 50,
                    StreetName = "Smith Lane",
                    Postcode = "XY15 8GG",
                    PricePerMonth = "£325 pcm"
                },
                new Property
                {
                    StreetNumber = 18,
                    StreetName = "Potter Row",
                    Postcode = "XY15 8GG",
                    PricePerMonth = "£615 pcm"
                },
                new Property
                {
                    StreetNumber = 12,
                    StreetName = "Alloway Grove",
                    Postcode = "XY15 8GG",
                    PricePerMonth = "£495 pcm"
                }
            };

            foreach(var p in propList)
            {
                await client.Child("Properties").PostAsync(p);
            }
        }

        public async Task AddToFavourites(Favourite favourite)
        {
            await client.Child("Favourites").PostAsync(favourite);
        }

        public async Task<List<Favourite>> GetAllFavourites()
        {
            return (await client.Child("Favourites")
            .OnceAsync<Favourite>()).Select(fav => new Favourite
            {
                Id = fav.Object.Id,
                Property = fav.Object.Property
            }).ToList();
        }

    }
}

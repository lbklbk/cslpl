﻿using CSLPropertyLetting.Interfaces;
using CSLPropertyLetting.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CSLPropertyLetting.Services
{
    public class StorageService : IStorageService
    {
        private DatabaseHelper _databaseHelper;
        public StorageService(DatabaseHelper databaseHelper)
        {
            _databaseHelper = databaseHelper;
        }
        public async Task<List<Favourite>> GetAllFavourites() => await _databaseHelper.GetAllFavourites();

        public async Task<List<Property>> GetAllProperties() => await _databaseHelper.GatherProperties();

        public async Task PopulateProperties() => await _databaseHelper.PopulateProperties();

        public async Task StoreFavourite(Favourite favourite) => await _databaseHelper.AddToFavourites(favourite);
    }
}

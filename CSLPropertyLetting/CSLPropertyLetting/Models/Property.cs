﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSLPropertyLetting.Models
{
    public class Property
    {
        public int StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string Postcode { get; set; }
        public string PricePerMonth { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSLPropertyLetting.Models
{
    public class Favourite
    {
        public Guid Id { get; set; }
        public Property Property { get; set; }
    }
}

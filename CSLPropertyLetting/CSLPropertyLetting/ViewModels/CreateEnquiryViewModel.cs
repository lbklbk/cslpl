﻿using CSLPropertyLetting.Models;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;

namespace CSLPropertyLetting.ViewModels
{
    public class CreateEnquiryViewModel : ViewModelBase
    {

        // Properties
        private Property _property;
        public Property Property
        {
            get { return _property; }
            set { SetProperty(ref _property, value); }
        }

        private string _streetName;
        public string StreetName
        {
            get { return _streetName; }
            set { SetProperty(ref _streetName, value); }
        }

        public CreateEnquiryViewModel(INavigationService navigationService) : base(navigationService)
        {
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters != null && parameters.ContainsKey("propertyModel"))
            {
                var activeProperty = parameters["propertyModel"] as Property;
                StreetName = activeProperty.StreetName;
            }
        }
    }
}

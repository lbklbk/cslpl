﻿using CSLPropertyLetting.Interfaces;
using CSLPropertyLetting.Models;
using Prism.Commands;
using Prism.Navigation;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials.Interfaces;

namespace CSLPropertyLetting.ViewModels
{
    public class PropertiesViewModel : ViewModelBase
    {
        // Dependencies
        IStorageService _storageService;

        // Properties
        private bool _busy;
        public bool Busy
        {
            get { return _busy; }
            set
            {
                if(SetProperty(ref _busy, value))
                {
                    RaisePropertyChanged(nameof(Busy));
                }
            }
        }

        private List<Property> _properties;
        public List<Property> Properties
        {
            get { return _properties; }
            set 
            { 
                if(SetProperty(ref _properties, value))
                {
                    RaisePropertyChanged(nameof(Properties));
                }
            }
        }

        private Property _selectedProperty;
        public Property SelectedProperty
        {
            get { return _selectedProperty; }
            set { SetProperty(ref _selectedProperty, value); }
        }

        public PropertiesViewModel(INavigationService navigationService, IStorageService storageService) : base(navigationService)
        {
            _storageService = storageService;
            Title = "CSL Home Letting";
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            Busy = true;
            Properties = await _storageService.GetAllProperties();
            Busy = false;
        }
    }
}

﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSLPropertyLetting.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        // Commands
        public DelegateCommand AuthenticateCommand { get; set; }

        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main Page";
            AuthenticateCommand = new DelegateCommand(AuthenticateUser);
        }

        private async void AuthenticateUser()
        {
            await NavigationService.NavigateAsync("app:///NavigationPage/PropertiesPage");
        }
    }
}
